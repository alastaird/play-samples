package controllers

import javax.inject.Inject
import play.api.Logging
import play.api.mvc.{AbstractController, ControllerComponents, Results}

import scala.concurrent.Future
import scala.concurrent.duration.Duration

final class DelayController @Inject() (cc: ControllerComponents) extends AbstractController(cc) with Logging {
  def get(length: String) = Action.async {
    logger.info("Delayed request started")
    Thread.sleep(Duration(length).toMillis)
    logger.info("Delayed request completed")
    Future.successful(Results.Ok)
  }
}
