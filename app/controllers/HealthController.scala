package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents, Results}

final class HealthController @Inject() (cc: ControllerComponents) extends AbstractController(cc) {
  def health(): Action[AnyContent] = Action {
    Results.Ok
  }
}
