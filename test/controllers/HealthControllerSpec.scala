package controllers

import java.util.concurrent.atomic.AtomicBoolean

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.inject.{BindingKey, Injector}
import play.api.test.Helpers._
import play.api.test._

final class HealthControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {
  "HealthController GET" should {
    "return OK in normal operation" in {
      val Some(home) = route(app, FakeRequest(GET, "/admin/health"))
      status(home) mustBe OK
    }

    "return SERVICE_UNAVAILABLE if shutting down" in  {
      val injector: Injector = inject[Injector]
      val isShuttingDown: AtomicBoolean = injector.instanceOf[AtomicBoolean](BindingKey(classOf[AtomicBoolean]).qualifiedWith("isShuttingDown"))
      isShuttingDown.getAndSet(true)

      val Some(home) = route(app, FakeRequest(GET, "/admin/health"))
      status(home) mustBe SERVICE_UNAVAILABLE
    }
  }
}
