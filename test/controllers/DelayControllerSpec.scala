package controllers

import java.time.Duration

import com.google.common.base.Stopwatch
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test.Helpers._
import play.api.test._

final class DelayControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {
  "Delayed response" should {
    "return a successful response" in {
      val Some(response) = route(app, FakeRequest(GET, "/delay?length=10ms"))

      status(response) mustBe OK
    }

    "wait for at least the requested time before responding" in {
      val stopwatch = Stopwatch.createStarted()
      val Some(response) = route(app, FakeRequest(GET, "/delay?length=1000ms"))

      status(response) mustBe OK
      stopwatch.stop()
      val elapsedDuration = stopwatch.elapsed()
      elapsedDuration.minus(Duration.ofMillis(1000)).toMillis() must be > 0L
    }
  }
}
