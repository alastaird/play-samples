
# Resources

/delay?length=xxx       where 'xxx' is a representation of a time parseable by `scala.concurrent.duration.Duration` companion object (e.g. 10s)

# Build

`sbt clean docker`

Produces a docker image 'org.adonlon/play-samples:latest'

# Run

`docker run -p 9000:9000 --rm --name play-samples org.adonlon/play-samples:latest`

# Test

`curl -v http://localhost:9000/delay?length=10s`

In a separate terminal I execute `docker stop -t 60 play-samples`
The response I see in the `curl` terminal is...10s

```
$ curl -v http://localhost:9000/delay?length=10s
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 9000 (#0)
> GET /delay?length=10s HTTP/1.1
> Host: localhost:9000
> User-Agent: curl/7.58.0
> Accept: */*
>
* Empty reply from server
* Connection #0 to host localhost left intact
curl: (52) Empty reply from server
```
